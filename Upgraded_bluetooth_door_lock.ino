#include<SoftwareSerial.h>
#include<Servo.h>

Servo myServo;
int bttx=10;    //tx of bluetooth module is connected to pin 9 of arduino
int btrx=11;    //rx of bluetooth module is connected to pin 10 of arduino
SoftwareSerial bluetooth(bttx,btrx);

int pos = 0; 


void setup()

{
 
  myServo.attach(9);        // servo is connected to pin 11 of arduino
  Serial.begin(9600);
  bluetooth.begin(9600);
  Serial.println("Initiating System..\n");
  delay(2000);
  Serial.println("System Online\n");
  myServo.write(90);
  
}

void loop(){

  if(bluetooth.available()>0 && bluetooth.read() == '1'){
    
    
    for (pos = 0; pos <= 360; pos += 1) { 
    myServo.write(pos);              
    delay(15); 
    myServo.write(90);                      
  }
   
    

    
  }else if (bluetooth.available()>0 && bluetooth.read() == '2'){

     
    for (pos = 360; pos >= 0; pos -= 1) {
    myServo.write(pos);              
    delay(15);                       
  }
      }
  
}
